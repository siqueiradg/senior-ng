import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class WorkflowService {

  private eventsSource = new BehaviorSubject<string>('');
  event =  this.eventsSource.asObservable();

  private responseSource = new BehaviorSubject<string>('');
  response =  this.responseSource.asObservable();

  constructor() { }

  sendEvent(event: string): void {
    this.eventsSource.next(event);
  }

  sendResponse(response: string): void {
    this.responseSource.next(response);
  }
}

