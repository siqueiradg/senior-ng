import { WorkflowService } from './../shared/services/workflow.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'form-two',
  templateUrl: 'form-two.component.html'
})

export class FormTwoComponent implements OnInit {

  print: string;

  constructor(private workflowService: WorkflowService) { }

  ngOnInit() {
    this.workflowService.event.subscribe(
      (data) => {
        this.print = data;
      }
    );
  }

  sendResponse() {
    this.workflowService.sendResponse('Resposta 2');
  }
}
