import { WorkflowService } from './shared/services/workflow.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit{

  response: string;

  constructor(private workflowService: WorkflowService){}

  ngOnInit(): void {
    this.workflowService.response.subscribe(
      (data) => {
        this.response = data;
      }
    );
  }

  sendEvent(): void {
    this.workflowService.sendEvent('saveData');
  }
}
