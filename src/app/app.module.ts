import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';
import { FormTwoComponent } from './form-two/form-two.component';
import { FormOneComponent } from './form-one/form-one.component';
import { FormThreeComponent } from './form-three/form-three.component';
import { WorkflowService } from './shared/services/workflow.service';


@NgModule({
  declarations: [
    AppComponent,
    FormOneComponent,
    FormTwoComponent,
    FormThreeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes)
  ],
  providers: [WorkflowService],
  bootstrap: [AppComponent]
})
export class AppModule { }
