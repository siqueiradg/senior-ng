import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';

import { FormTwoComponent } from './form-two/form-two.component';
import { FormOneComponent } from './form-one/form-one.component';
import { FormThreeComponent } from './form-three/form-three.component';
import { AppComponent } from './app.component';

export const AppRoutes: Routes = [
  {
      path: '', redirectTo: 'form-one', pathMatch: 'full'
  },
  {
    // -> pai
    path: '',
    // component: AppComponent,
    children: [
      // -> filhos
      {
        path: 'form-one',
        component: FormOneComponent
      },
      {
        path: 'form-two',
        component: FormTwoComponent
      },
      {
        path: 'form-three',
        component: FormThreeComponent
      }
    ]
  }
];
