import { Component, OnInit } from '@angular/core';
import { WorkflowService } from '../shared/services/workflow.service';

@Component({
  selector: 'form-one',
  templateUrl: 'form-one.component.html'
})

export class FormOneComponent implements OnInit {

  print: string;

  constructor(private workflowService: WorkflowService) { }

  ngOnInit() {
    this.workflowService.event.subscribe(
      (data) => {
        this.print = data;
      }
    );
  }

  sendResponse() {
    this.workflowService.sendResponse('Resposta 1');
  }

}
